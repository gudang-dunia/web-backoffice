const Home = () => {
    return `https://covid-19-data.p.rapidapi.com/country/all`;
}

const SignIn = () => {
    return `/api-oauth-service/oauth/v1/signin`;
}

const SignUp = () => {
    return `/api-oauth-service/oauth/v1/signin`;
}

const RefreshToken = () => {
    return `/api-oauth-service/oauth/v1/refresh`;
}

const CurrentAccount = () => {
    return `/api-oauth-service/oauth/v1/me`;
}

const GetAccount = (username) => {
    return `/api-oauth-service/oauth/v1/${username}`;
}

const CreateItem = () => {
    return `/api-oauth-service/master/item/v1/save`;
}

const ImportItem = () => {
    return `/api-oauth-service/master/item/v1/import`;
}

const ListItem = (column, status, page, size, sort='') => {
    return `/api-oauth-service/master/item/v1/search?column=${column}&status=${status}&page=${page}&size=${size}&sort=${sort}`;
}

const ImportCompany = () => {
    return `/api-oauth-service/master/company/v1/import`;
}

const CreateCompany = () => {
    return `/api-oauth-service/master/company/v1/save`;
}

const ListCompany = (column, status, page, size, sort='') => {
    return `/api-oauth-service/master/company/v1/search?column=${column}&status=${status}&page=${page}&size=${size}&sort=${sort}`;
}

const CreateUser = () => {
    return `/api-oauth-service/oauth/v1/signup`;
}

const GetItemById = (id) => {
    return `/api-oauth-service/master/item/v1/${id}`;
}

const UpdateItem = () => {
    return `/api-oauth-service/master/item/v1/update`;
}

const GetCompanyById = (id) => {
    return `/api-oauth-service/master/company/v1/${id}`;
}

const UpdateCompany = () => {
    return `/api-oauth-service/master/company/v1/update`;
}

const GenerateQRCode = () => {
    return `/api-oauth-service/master/barcode/v1/generade-qr`;
}

const ListHeader = (column, date, status, page, size, sort='') => {
    return `/api-oauth-service/master/barcode/v1/data-header?column=${column}&barcode_date=${date}&status=${status}&page=${page}&size=${size}&sort=${sort}`;
}

const DownloadHeader = (header) => {
    return `/api-oauth-service/master/barcode/v1/zip-qr/${header}`;
}

module.exports = {
    Home,
    SignIn,
    SignUp,
    RefreshToken,
    CurrentAccount,
    GetAccount,
    CreateUser,
    CreateItem,
    ImportItem,
    ImportCompany,
    CreateCompany,
    ListItem,
    ListCompany,
    GetItemById,
    UpdateItem,
    GetCompanyById,
    UpdateCompany,
    GenerateQRCode,
    ListHeader,
    DownloadHeader,
}
