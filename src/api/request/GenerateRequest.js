import axios from "axios";
import { GenerateQRCode, ListHeader, DownloadHeader } from "@/api/endpoint";
import { BaseUrl } from "@/api/endpoint/BaseUrl";
import { getToken } from '@/utils/cookies.js'

const headers = {
    'Authorization': `Bearer ${getToken()}`,
};

const headersUploadFile = {
    'Authorization': `Bearer ${getToken()}`,
    'Content-Type': 'multipart/form-data',
};

export const generateQRCodeRequest = (
    companyCode,
    itemCode,
    productEnd,
    productStart,
    urlLink,
    urlLogo,
) => {
    const resp = axios({
        method: 'post',
        data: {
            company_code: companyCode,
            item_code: itemCode,
            product_end: productEnd,
            product_start: productStart,
            url_link: urlLink,
            url_logo: urlLogo,
        },
        url: `${BaseUrl()}${GenerateQRCode()}`,
    }).then((response) => {
        return response;
    });
    return resp;
}

export const listHeaderRequest = (column, date, status, page, size, sort) => {
    const resp = axios({
        method: 'get',
        url: `${BaseUrl()}${ListHeader(column, date, status, page, size, sort)}`,
    }).then((response) => {
        return response;
    });
    return resp;
}

export const downloadHeaderRequest = (header) => {
    const resp = axios({
        method: 'get',
        url: `${BaseUrl()}${DownloadHeader(header)}`,
    }).then(() => {
        return `${BaseUrl()}${DownloadHeader(header)}`;
    });
    return resp;
}

