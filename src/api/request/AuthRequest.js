import axios from "axios";
import { SignIn, SignUp, GetAccount, CreateUser, RefreshToken, CurrentAccount } from "@/api/endpoint";
import { BaseUrl } from "@/api/endpoint/BaseUrl";
import { getToken } from '@/utils/cookies.js'
const headers = {
    'Authorization': `Bearer ${getToken()}`,
};

export const AuthRequest = (username, password) => {
    const resp = axios({
        method: 'post',
        data: {
            username: username,
            password: password,
        },
        url: `${BaseUrl()}${SignIn()}`,
    }).then((response) => {
        return response;
    });
    return resp;
}

export const SignUpRequest = (name, username, password, email, roles) => {
    const resp = axios({
        method: 'post',
        data: {
            name: name,
            email: email,
            roles: roles,
            username: username,
            password: password,
        },
        url: `${BaseUrl()}${SignUp()}`,
    }).then((response) => {
        return response;
    });
    return resp;
}

export const RefreshTokenRequest = () => {
    const resp = axios({
        method: 'get',
        url: `${BaseUrl()}${RefreshToken()}`,
        headers:  headers,
    }).then((response) => {
        return response;
    });
    return resp;
}

export const CurrentAccountRequest = () => {
    const resp = axios({
        method: 'get',
        url: `${BaseUrl()}${CurrentAccount()}`,
        headers:  headers,
    }).then((response) => {
        return response;
    });
    return resp;
}

export const AccountRequest = (username) => {
    const resp = axios({
        method: 'get',
        url: `${BaseUrl()}${GetAccount(username)}`,
        headers:  headers,
    }).then((response) => {
        return response;
    });
    return resp;
}

export const CreateUserRequest = (
    username,
    email,
    password,
    roles,
) => {
    const resp = axios({
        method: 'post',
        data: {
            email: email,
            password: password,
            roles: roles,
            username: username,
        },
        url: `${BaseUrl()}${CreateUser()}`,
    }).then((response) => {
        return response;
    });
    return resp;
}
