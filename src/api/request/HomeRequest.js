import axios from "axios";
import { Home } from "@/api/endpoint";

const headers = {
    'x-rapidapi-host': 'covid-19-data.p.rapidapi.com',
    'x-rapidapi-key': '83aeeed1c8mshe9865a7f9b4a9c4p13c789jsn09af56732f64',
}
export const HomeRequest = () => {
    const resp = axios({
        method: 'get',
        url: Home(),
        headers: headers,
    }).then((response) => {
        return response;
    });
    return resp;
}