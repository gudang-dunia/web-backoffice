import axios from "axios";
import { ImportCompany, CreateCompany, ListCompany, GetCompanyById, UpdateCompany } from "@/api/endpoint";
import { BaseUrl } from "@/api/endpoint/BaseUrl";
import { getToken } from '@/utils/cookies.js'

const headers = {
    'Authorization': `Bearer ${getToken()}`,
};

const headersUploadFile = {
    'Authorization': `Bearer ${getToken()}`,
    'Content-Type': 'multipart/form-data',
};

export const createCompanyRequest = (
    activeStatus,
    address,
    cityCode,
    code,
    countryCode,
    emailAddress,
    fax,
    logoPath,
    name,
    npwp,
    npwpAddress,
    phone1,
    warehouseCode,
    zipCode,
) => {
    const resp = axios({
        method: 'post',
        data: {
            activeStatus: activeStatus,
            address: address,
            cityCode: cityCode,
            code: code,
            countryCode: countryCode,
            emailAddress: emailAddress,
            fax: fax,
            logoPath: logoPath,
            name: name,
            npwp: npwp,
            npwpAddress: npwpAddress,
            phone1: phone1,
            warehouseCode: warehouseCode,
            zipCode: zipCode,
        },
        url: `${BaseUrl()}${CreateCompany()}`,
    }).then((response) => {
        return response;
    });
    return resp;
}

export const importCompanyRequest = (importFile) => {
    const bodyFormData = new FormData()
    bodyFormData.append('file', importFile)

    const resp = axios({
        method: 'post',
        data: bodyFormData,
        url: `${BaseUrl()}${ImportCompany()}`,
    }).then((response) => {
        return response;
    });
    return resp;
}

export const getCompanyByIdRequest = (id) => {
    const resp = axios({
        method: 'get',
        url: `${BaseUrl()}${GetCompanyById(id)}`,
    }).then((response) => {
        return response;
    });
    return resp;
}

export const ListCompanyRequest = (column, status, page, size, sort) => {
    const resp = axios({
        method: 'get',
        url: `${BaseUrl()}${ListCompany(column, status, page, size, sort)}`,
    }).then((response) => {
        return response;
    });
    return resp;
}

export const updateCompanyRequest = (
    activeStatus,
    address,
    cityCode,
    code,
    countryCode,
    emailAddress,
    fax,
    logoPath,
    name,
    npwp,
    npwpAddress,
    phone1,
    warehouseCode,
    zipCode,
) => {
    const resp = axios({
        method: 'post',
        data: {
            activeStatus: activeStatus,
            address: address,
            cityCode: cityCode,
            code: code,
            countryCode: countryCode,
            emailAddress: emailAddress,
            fax: fax,
            logoPath: logoPath,
            name: name,
            npwp: npwp,
            npwpAddress: npwpAddress,
            phone1: phone1,
            warehouseCode: warehouseCode,
            zipCode: zipCode,
        },
        url: `${BaseUrl()}${UpdateCompany()}`,
    }).then((response) => {
        return response;
    });
    return resp;
}