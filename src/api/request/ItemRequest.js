import axios from "axios";
import { CreateItem, ImportItem, ListItem, GetItemById, UpdateItem } from "@/api/endpoint";
import { BaseUrl } from "@/api/endpoint/BaseUrl";
import { getToken } from '@/utils/cookies.js'

const headers = {
    'Authorization': `Bearer ${getToken()}`,
};

const headersUploadFile = {
    'Authorization': `Bearer ${getToken()}`,
    'Content-Type': 'multipart/form-data',
};

export const createItemRequest = (
    activeStatus,
    basedPrice,
    code,
    companyCode,
    name,
    normalPrice,
    remark,
    volume,
) => {
    const resp = axios({
        method: 'post',
        data: {
            activeStatus: activeStatus,
            basedPrice: basedPrice,
            code: code,
            companyCode: companyCode,
            name: name,
            normalPrice: normalPrice,
            remark: remark,
            volume: volume,
        },
        url: `${BaseUrl()}${CreateItem()}`,
    }).then((response) => {
        return response;
    });
    return resp;
}

export const importItemRequest = (importFile) => {
    const bodyFormData = new FormData()
    bodyFormData.append('file', importFile)

    const resp = axios({
        method: 'post',
        data: bodyFormData,
        url: `${BaseUrl()}${ImportItem()}`,
    }).then((response) => {
        return response;
    });
    return resp;
}

export const listItemRequest = (column, status, page, size, sort) => {
    const resp = axios({
        method: 'get',
        url: `${BaseUrl()}${ListItem(column, status, page, size, sort)}`,
    }).then((response) => {
        return response;
    });
    return resp;
}

export const getItemByIdRequest = (id) => {
    const resp = axios({
        method: 'get',
        url: `${BaseUrl()}${GetItemById(id)}`,
    }).then((response) => {
        return response;
    });
    return resp;
}

export const updateItemRequest = (
    activeStatus,
    basedPrice,
    code,
    companyCode,
    name,
    normalPrice,
    remark,
    volume,
) => {
    const resp = axios({
        method: 'post',
        data: {
            activeStatus: activeStatus,
            basedPrice: basedPrice,
            code: code,
            companyCode: companyCode,
            name: name,
            normalPrice: normalPrice,
            remark: remark,
            volume: volume,
        },
        url: `${BaseUrl()}${UpdateItem()}`,
    }).then((response) => {
        return response;
    });
    return resp;
}