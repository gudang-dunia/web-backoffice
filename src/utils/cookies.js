import Cookies from 'js-cookie'

export const setAuthentication = (token) => {
    Cookies.set('back_office', token)
}

export const getToken = () => Cookies.get('back_office') || ''

export const removeAuth = () => {
    Cookies.remove('back_office')
}
