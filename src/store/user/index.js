import router from '@/router'
import { notification } from 'ant-design-vue'

import * as jwt from '@/services/jwt'

const mapAuthProviders = {
  jwt: {
    login: jwt.login,
    register: jwt.register,
    currentAccount: jwt.currentAccount,
    logout: jwt.logout,
  },
}

export default {
  namespaced: true,
  state: {
    id: '',
    email: '',
    roles: '',
    username: '',
    avatar: '',
    authorized: false,
    loading: false,
    accountFetchIsTouched: false,
    // ...DEV, // remove it, used for demo build
  },
  mutations: {
    SET_STATE(state, payload) {
      Object.assign(state, {
        ...payload,
      })
    },
  },
  actions: {
    LOGIN({ commit, dispatch, rootState }, { payload }) {
      const { username, password } = payload
      commit('SET_STATE', {
        loading: true,
      })

      const login = mapAuthProviders[rootState.settings.authProvider].login
      login(username, password).then(success => {
        if (success) {
          dispatch('LOAD_CURRENT_ACCOUNT')
          notification.success({
            message: 'Logged In',
            description: 'You have successfully logged in!',
          })
        }
        if (!success) {
          commit('SET_STATE', {
            loading: false,
          })
          notification.warning({
            message: 'Logged Failed',
            description: 'username or password fail!',
          })
        }
      })
    },
    REGISTER({ commit, dispatch, rootState }, { payload }) {
      const { name, username, password, email } = payload
      commit('SET_STATE', {
        loading: true,
      })

      const register = mapAuthProviders[rootState.settings.authProvider].register
      register(name, username, password, email).then(success => {
        if (success) {
          dispatch('LOAD_CURRENT_ACCOUNT')
          notification.success({
            message: 'Succesful Registered',
            description: 'You have successfully registered!',
          })
        }
        if (!success) {
          commit('SET_STATE', {
            loading: false,
          })
        }
      })
    },
    LOAD_CURRENT_ACCOUNT({ commit, rootState }) {
      commit('SET_STATE', {
        loading: true,
      })

      const currentAccount = mapAuthProviders[rootState.settings.authProvider].currentAccount
      currentAccount().then(response => {
        if (response) {
          const { id, username, email, avatar, roles } = response
          commit('SET_STATE', {
            id,
            email,
            username,
            avatar,
            roles,
            authorized: true,
          })
        }
        commit('SET_STATE', {
          loading: false,
        })
        router.push('/auth/login')
      })
    },
    LOGOUT({ commit, rootState }) {
      const logout = mapAuthProviders[rootState.settings.authProvider].logout
      notification.success({
        message: 'Succes logout',
        description: 'You have successfully logout system!',
      })
      logout().then(() => {
        commit('SET_STATE', {
          id: '',
          email: '',
          roles: '',
          username: '',
          avatar: '',
          authorized: false,
          loading: false,
        })
        router.push('/auth/login')
      })
    },
  },
  getters: {
    user: state => state,
  },
}
