import { AuthRequest, SignUpRequest, AccountRequest, CreateUserRequest, RefreshTokenRequest, CurrentAccountRequest } from "../api/request/AuthRequest";

export class AuthControllers {
    loading = false;
    error = false;

    constructor(loading, error) {
        this.loading = loading;
        this.error = error;
    }

    signIn(username, password) {
        this.setLoading(true);
        const resp = AuthRequest(username, password)
            .then((response) => {
                return response;
            }).catch(() => {
                this.setError(true);
            }).finally(() => {
                this.setLoading(false);
            });

        return resp;
    }

    signUp(name, username, password, email) {
        this.setLoading(true);
        const resp = SignUpRequest(name, username, password, email)
            .then((response) => {
                return response;
            }).catch(() => {
                this.setError(true);
            }).finally(() => {
                this.setLoading(false);
            });

        return resp;
    }

    refreshToken() {
        this.setLoading(true);
        const resp = RefreshTokenRequest()
            .then((response) => {
                return response;
            }).catch(() => {
                this.setError(true);
            }).finally(() => {
                this.setLoading(false);
            });

        return resp;
    }

    currentAccount() {
        this.setLoading(true);
        const resp = CurrentAccountRequest()
            .then((response) => {
                return response;
            }).catch(() => {
                this.setError(true);
            }).finally(() => {
                this.setLoading(false);
            });

        return resp;
    }

    getAccount(username) {
        this.setLoading(true);
        const resp = AccountRequest(username)
            .then((response) => {
                return response;
            }).catch(() => {
                this.setError(true);
            }).finally(() => {
                this.setLoading(false);
            });

        return resp;
    }

    createUser(username, email, password, roles) {
        this.setLoading(true);
        const resp = CreateUserRequest(username, email, password, roles)
            .then((response) => {
                return response;
            }).catch(() => {
                this.setError(true);
            }).finally(() => {
                this.setLoading(false);
            });

        return resp;
    }

    setLoading(status) {
        this.loading = status;
    }

    setError(err) {
        this.error = err;
    }
}
