import { importCompanyRequest, createCompanyRequest, ListCompanyRequest, getCompanyByIdRequest, updateCompanyRequest } from "../api/request/CompanyRequest";

export class CompanyControllers {
    loading = false;
    error = false;
    errorCause = "";
    dataItems = [];
    detailCompany = {};

    constructor(loading, error, errorCause) {
        this.loading = loading;
        this.error = error;
        this.errorCause = errorCause;
    }

    importCompany(importFile) {
        this.setLoading(true);
        const resp = importCompanyRequest(importFile)
            .then((response) => {
                this.setError(false);
                this.setErrorCause('')
                return response;
            }).catch((err) => {
                this.setError(true);
                this.setErrorCause(err)
            }).finally(() => {
                this.setLoading(false);
            });
        
        return resp;
    }

    listCompany(column, status, page, size, sort) {
        this.setLoading(true);
        const resp = ListCompanyRequest(column, status, page, size, sort)
            .then((response) => {
                this.setError(false);
                this.setErrorCause('')
                this.setDataItems(response.data.data)
                return response;
            }).catch((err) => {
                this.setError(true);
                this.setErrorCause(err)
            }).finally(() => {
                this.setLoading(false);
            });
        
        return resp;
    }

    getCompanyById(id) {
        this.setLoading(true);
        const resp = getCompanyByIdRequest(id)
            .then((response) => {
                this.setError(false);
                this.setErrorCause('')
                this.setCompanyDetail(response.data.data)
                return response;
            }).catch((err) => {
                this.setError(true);
                this.setErrorCause(err)
            }).finally(() => {
                this.setLoading(false);
            });
        
        return resp;
    }

    createCompany(
        activeStatus,
        address,
        cityCode,
        code,
        countryCode,
        emailAddress,
        fax,
        logoPath,
        name,
        npwp,
        npwpAddress,
        phone1,
        warehouseCode,
        zipCode,
    ) {
        this.setLoading(true);
        const resp = createCompanyRequest(
            activeStatus,
            address,
            cityCode,
            code,
            countryCode,
            emailAddress,
            fax,
            logoPath,
            name,
            npwp,
            npwpAddress,
            phone1,
            warehouseCode,
            zipCode,
        )
            .then((response) => {
                this.setError(false);
                this.setErrorCause('')
                return response;
            }).catch((err) => {
                this.setError(true);
                this.setErrorCause(err)
            }).finally(() => {
                this.setLoading(false);
            });
        
        return resp;
    }

    updateCompany(
        activeStatus,
        address,
        cityCode,
        code,
        countryCode,
        emailAddress,
        fax,
        logoPath,
        name,
        npwp,
        npwpAddress,
        phone1,
        warehouseCode,
        zipCode,
    ) {
        this.setLoading(true);
        const resp = updateCompanyRequest(
            activeStatus,
            address,
            cityCode,
            code,
            countryCode,
            emailAddress,
            fax,
            logoPath,
            name,
            npwp,
            npwpAddress,
            phone1,
            warehouseCode,
            zipCode,
        )
            .then((response) => {
                this.setError(false);
                this.setErrorCause('')
                return response;
            }).catch((err) => {
                this.setError(true);
                this.setErrorCause(err)
            }).finally(() => {
                this.setLoading(false);
            });
        
        return resp;
    }

    setDataItems(data) {
        this.dataItems = data;
    }

    setCompanyDetail(data) {
        this.detailCompany = data;
    }

    setLoading(status) {
        this.loading = status;
    }

    setError(err) {
        this.error = err;
    }

    setErrorCause(err) {
        this.errorCause= err;
    }
}
