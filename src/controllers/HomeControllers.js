import { HomeRequest } from "@/api/request/HomeRequest"

export class HomeControllers {
    movies = [];
    loading = false;
    error = false;

    constructor(movies, loading, error) {
        this.movies = movies;
        this.loading = loading;
        this.error = error;
    }

    getTitle() {
        return "Hello IKB"
    }

    getMovies() {
        this.setLoading(true);
        const resp = HomeRequest()
            .then((response) => {
                this.setMovies(response);
            }).catch(() => {
                this.setError(true);
            }).finally(() => {
                this.setLoading(false);
            });
        
        return resp;
    }

    setMovies(data) {
        this.movies = data;
    }

    setError(status) {
        this.error = status
    }

    setLoading(status) {
        this.loading = status;
    }
}