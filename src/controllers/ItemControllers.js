import { createItemRequest, importItemRequest, listItemRequest, getItemByIdRequest, updateItemRequest } from "../api/request/ItemRequest";

export class ItemControllers {
    loading = false;
    error = false;
    errorCause = "";
    dataItems = [];
    detailItem = {};

    constructor(loading, error, errorCause) {
        this.loading = loading;
        this.error = error;
        this.errorCause = errorCause;
    }

    importItem(importFile) {
        this.setLoading(true);
        const resp = importItemRequest(importFile)
            .then((response) => {
                this.setError(false);
                this.setErrorCause('')
                return response;
            }).catch((err) => {
                this.setError(true);
                this.setErrorCause(err)
            }).finally(() => {
                this.setLoading(false);
            });
        
        return resp;
    }

    listItem(column, status, page, size, sort) {
        this.setLoading(true);
        const resp = listItemRequest(column, status, page, size, sort)
            .then((response) => {
                this.setError(false);
                this.setErrorCause('')
                this.setDataItems(response.data.data)
                return response;
            }).catch((err) => {
                this.setError(true);
                this.setErrorCause(err)
            }).finally(() => {
                this.setLoading(false);
            });
        
        return resp;
    }

    getItemById(id) {
        this.setLoading(true);
        const resp = getItemByIdRequest(id)
            .then((response) => {
                this.setError(false);
                this.setErrorCause('')
                this.setItemDetail(response.data.data)
                return response;
            }).catch((err) => {
                this.setError(true);
                this.setErrorCause(err)
            }).finally(() => {
                this.setLoading(false);
            });
        
        return resp;
    }

    createItem(
        activeStatus,
        basedPrice,
        code,
        companyCode,
        name,
        normalPrice,
        remark,
        volume,
    ) {
        this.setLoading(true);
        const resp = createItemRequest(
            activeStatus,
            basedPrice,
            code,
            companyCode,
            name,
            normalPrice,
            remark,
            volume,
        )
            .then((response) => {
                this.setError(false);
                this.setErrorCause('')
                return response;
            }).catch((err) => {
                this.setError(true);
                this.setErrorCause(err)
            }).finally(() => {
                this.setLoading(false);
            });
        
        return resp;
    }

    updateItem(
        activeStatus,
        basedPrice,
        code,
        companyCode,
        name,
        normalPrice,
        remark,
        volume,
    ) {
        this.setLoading(true);
        const resp = updateItemRequest(
            activeStatus,
            basedPrice,
            code,
            companyCode,
            name,
            normalPrice,
            remark,
            volume,
        )
            .then((response) => {
                this.setError(false);
                this.setErrorCause('')
                return response;
            }).catch((err) => {
                this.setError(true);
                this.setErrorCause(err)
            }).finally(() => {
                this.setLoading(false);
            });
        
        return resp;
    }

    setDataItems(data) {
        this.dataItems = data;
    }

    setItemDetail(data) {
        this.detailItem = data;
    }

    setLoading(status) {
        this.loading = status;
    }

    setError(err) {
        this.error = err;
    }

    setErrorCause(err) {
        this.errorCause= err;
    }
}
