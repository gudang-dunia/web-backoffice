import { generateQRCodeRequest, listHeaderRequest, downloadHeaderRequest } from "../api/request/GenerateRequest";

export class GenerateControllers {
    loading = false
    error = false;
    errorCause = "";
    dataItems = [];
    loadingDownload = false
    zipUrl = ''
    
    constructor(loading, error, errorCause, dataItems, loadingDownload, zipUrl) {
        this.loading = loading;
        this.error = error;
        this.errorCause = errorCause;
        this.dataItems = dataItems;
        this.loadingDownload = loadingDownload;
        this.zipUrl = zipUrl
    }

    generateQRCode(
        companyCode,
        itemCode,
        productEnd,
        productStart,
        urlLink,
        urlLogo,
    ) {
        this.setLoading(true);
        const resp = generateQRCodeRequest(
            companyCode,
            itemCode,
            productEnd,
            productStart,
            urlLink,
            urlLogo,
        )
            .then((response) => {
                this.setError(false);
                this.setErrorCause('')
                return response;
            }).catch((err) => {
                this.setError(true);
                this.setErrorCause(err)
            }).finally(() => {
                this.setLoading(false);
            });
        
        return resp;
    }

    listHeader(column, date, status, page, size, sort) {
        this.setLoading(true);
        const resp = listHeaderRequest(column, date, status, page, size, sort)
            .then((response) => {
                this.setError(false);
                this.setErrorCause('')
                this.setDataItems(response.data.data)
                return response;
            }).catch((err) => {
                this.setError(true);
                this.setErrorCause(err)
            }).finally(() => {
                this.setLoading(false);
            });
        
        return resp;
    }

    downloadHeader(header) {
        this.setLoadingDownload(true);
        const resp = downloadHeaderRequest(header)
            .then((response) => {
                this.setError(false);
                this.setErrorCause('')
                this.setZipUrl(response)
                return response;
            }).catch((err) => {
                this.setError(true);
                this.setErrorCause(err)
            }).finally(() => {
                this.setLoadingDownload(false);
            });
        
        return resp;
    }

    setDataItems(data) {
        this.dataItems = data;
    }

    setLoading(status) {
        this.loading = status;
    }

    setError(err) {
        this.error = err;
    }

    setErrorCause(err) {
        this.errorCause= err;
    }

    setLoadingDownload(status) {
        this.loadingDownload = status;
    }

    setZipUrl(url) {
        this.zipUrl = url
    }
}
