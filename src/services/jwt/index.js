import { setAuthentication, removeAuth } from '@/utils/cookies.js'

import { AuthControllers } from '../../controllers/AuthControllers'
const auth = new AuthControllers(false, false);

export async function login(username, password) {
  return auth.signIn(username, password).then(response => {
    if (response) {
      setAuthentication(response.data)
      return response;
    }
    return false
  }).catch(err => console.log(err))
}

export async function register(name, username, password, email) {
  const roles = ['ROLE_CLIENT']
  removeAuth()
  return auth.signUp(name, username, password, email, roles)
    .then(response => {
      if (response) {
        setAuthentication(response.data)
        return response;
      }
      return false
    })
    .catch(err => console.log(err))
}

export async function refreshToken() {
  return auth.refreshToken().then(response => {
    if (response) {
      setAuthentication(response.data)
      return response;
    }
    return false
  }).catch(err => console.log(err))
}

export async function currentAccount() {
  return auth.currentAccount().then(response => {
      if (response) {
        const result  = JSON.parse(JSON.stringify(response))
        return result.data
      }
      return false
    })
    .catch(err => console.log(err))
}

export async function logout() {
  removeAuth()
}
