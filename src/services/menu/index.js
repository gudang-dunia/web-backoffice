export const getMenuData = [
  // VB:REPLACE-START:MENU-CONFIG
  {
    title: 'Home',
    key: 'home',
    url: '/home',
    icon: 'fe fe-home',
  },
  {
    title: 'Master',
    key: 'master',
    url: '/master',
    icon: 'fe fe-database',
    children: [
      {
        title: 'Company',
        key: 'company',
        url: '/master/company/0',
      },
      {
        title: 'Item',
        key: 'item',
        url: '/master/item/0',
      },
    ],
  },
  {
    title: 'Generate QRCode',
    key: 'generate-qr-code',
    url: '/generate/0',
    icon: 'fe fe-file',
  },
  {
    title: 'Security',
    key: 'security',
    url: '/security',
    icon: 'fe fe-users',
    children: [
      {
        title: 'User',
        key: 'user',
        url: '/security/user',
      },
    ],
  },
  // VB:REPLACE-END:MENU-CONFIG
]
