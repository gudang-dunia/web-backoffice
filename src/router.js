import { createRouter, createWebHashHistory } from 'vue-router'
import NProgress from 'nprogress'
import AuthLayout from '@/layouts/Auth'
import MainLayout from '@/layouts/Main'
import { getToken, removeAuth } from '@/utils/cookies.js'

const router = createRouter({
  base: process.env.BASE_URL,
  scrollBehavior() {
    return { x: 0, y: 0 }
  },
  history: createWebHashHistory(),
  routes: [
    {
      path: '/',
      name: 'Home',
      redirect: '/home',
      component: MainLayout,
      meta: {
        authRequired: true,
        hidden: true,
      },
      children: [
        {
          path: '/home',
          meta: { title: 'Home' },
          component: () => import('./views/home'),
        },
      ],
    },
    {
      path: '/master',
      name: 'Master',
      redirect: '/master',
      component: MainLayout,
      meta: {
        authRequired: true,
        hidden: true,
      },
      children: [
        {
          path: '/master/company/:page',
          meta: { title: 'Company' },
          component: () => import('./views/master/company'),
        },
        {
          path: '/master/company/add',
          name: 'Create Company',
          meta: { title: 'Create Company' },
          component: () => import('./views/master/company/add'),
        },
        {
          path: '/master/company/:page/:id',
          name: 'Detail Company',
          meta: { title: 'Detail Company' },
          component: () => import('./views/master/company/detail'),
        },
        {
          path: '/master/item/:page',
          meta: { title: 'Item' },
          component: () => import('./views/master/item'),
        },
        {
          path: '/master/item/add',
          name: 'Create Item',
          meta: { title: 'Create Item' },
          component: () => import('./views/master/item/add'),
        },
        {
          path: '/master/item/:page/:id',
          name: 'Detail Item',
          meta: { title: 'Detail Item' },
          component: () => import('./views/master/item/detail'),
        },
      ],
    },
    {
      path: '/generate',
      name: 'Generate QRCode',
      redirect: '/generate',
      component: MainLayout,
      meta: {
        authRequired: true,
        hidden: true,
      },
      children: [
        {
          path: '/generate/:page',
          meta: { title: 'Generate QRCode' },
          component: () => import('./views/generate'),
        },
        {
          path: '/generate/add',
          name: 'Create QRCode',
          meta: { title: 'Generate QRCode' },
          component: () => import('./views/generate/add'),
        },
      ],
    },
    {
      path: '/security',
      name: 'Security',
      redirect: '/security',
      component: MainLayout,
      meta: {
        authRequired: true,
        hidden: true,
      },
      children: [
        {
          path: '/security/user',
          meta: { title: 'User' },
          component: () => import('./views/security/user'),
        },
        {
          path: '/security/user/add',
          name: 'Create User',
          meta: { title: 'Create User' },
          component: () => import('./views/security/user/add'),
        },
      ],
    },
    // System Pages
    {
      path: '/auth',
      component: AuthLayout,
      redirect: 'auth/login',
      children: [
        {
          path: '/auth/404',
          name: 'route404',
          meta: {
            title: 'Error 404',
          },
          component: () => import('./views/auth/404'),
        },
        {
          path: '/auth/500',
          meta: {
            title: 'Error 500',
          },
          component: () => import('./views/auth/500'),
        },
        {
          path: '/auth/login',
          name: 'login',
          meta: {
            title: 'Sign In',
          },
          component: () => import('./views/auth/login'),
        },
        {
          path: '/auth/register',
          meta: {
            title: 'Sign Up',
          },
          component: () => import('./views/auth/register'),
        },
        {
          path: '/auth/forgot-password',
          meta: {
            title: 'Forgot Password',
          },
          component: () => import('./views/auth/forgot-password'),
        },
        {
          path: '/auth/lockscreen',
          meta: {
            title: 'Lockscreen',
          },
          component: () => import('./views/auth/lockscreen'),
        },
      ],
    },

    // Redirect to 404
    {
      path: '/:pathMatch(.*)*',
      redirect: { name: 'route404' },
    },
  ],
})

router.beforeEach((to, from, next) => {
  NProgress.start()
  setTimeout(() => {
    NProgress.done()
  }, 300)

  function checkAuth() {
    const token = getToken();
    return token
  }
  const isAuth = checkAuth();

  if (to.name !== "login" && !isAuth) {
    removeAuth();
    next({ name: 'login' });
    return;
  }

  if (to.name === "login" && isAuth) {
    next("home");
    return;
  }

  next()
  
})

export default router
